package in.weargenius.accounts;

import java.io.FileReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import facebook4j.Facebook;
import facebook4j.FacebookFactory;
import facebook4j.auth.AccessToken;
import in.weargenius.gui.MainGUI;

public class InitializeAccounts {
	private Object FB_APPID;
	private Object FB_SECRET;
	private Object FB_TOKEN;
	private Object FB_PERMISSIONS;
	private Object TW_CKEY;
	private Object TW_CSECRET;
	private Object TW_TOKEN;
	private Object TW_TSECRET;
	public void addKeys(){
		
	}
	public static void main(String[] args){
		new InitializeAccounts();
	}
	public InitializeAccounts(){
		JSONParser parser = new JSONParser();
		 
		try {

			Object obj = parser.parse(new FileReader("D:\\socialconfig.json"));
			//Format: {"facebook":[{"ID":"4343343"},{"SECRET":"strrt"},{"TOKEN":"dgfgfg"},{"PERMISSIONS":"dhfjdhf"}],"twitter":[{"CKEY":"vdfd"},{"CSECRET":"dfdf"},{"TOKEN":"dfdf"},{"TSECRET":"dfdf"}]}
			JSONObject jsonObject = (JSONObject) obj;

			JSONArray facebookConfig = (JSONArray) jsonObject.get("facebook");
			JSONObject facebookObj= (JSONObject) facebookConfig.get(0);
			FB_APPID=facebookObj.get("ID");
						facebookObj= (JSONObject) facebookConfig.get(1);
			FB_SECRET=facebookObj.get("SECRET");
						facebookObj= (JSONObject) facebookConfig.get(2);
			FB_TOKEN=facebookObj.get("TOKEN");
						facebookObj= (JSONObject) facebookConfig.get(3);
			FB_PERMISSIONS=facebookObj.get("PERMISSIONS");
			
			JSONArray twitterconfig = (JSONArray) jsonObject.get("twitter");
			JSONObject twitterobj= (JSONObject) twitterconfig.get(0);
			TW_CKEY=twitterobj.get("CKEY");
			twitterobj= (JSONObject) twitterconfig.get(1);
			TW_CSECRET=twitterobj.get("CSECRET");
			twitterobj= (JSONObject) twitterconfig.get(2);
			TW_TOKEN=twitterobj.get("TOKEN");
			twitterobj= (JSONObject) twitterconfig.get(3);
			TW_TSECRET=twitterobj.get("TSECRET");
		} catch (Exception e) {
			e.printStackTrace();
			MainGUI.log("No authentication files Found.",true);
		}
	}
	public Facebook getFBWithCredentials(){
		Facebook facebook = new FacebookFactory().getInstance();
		facebook.setOAuthAppId(FB_APPID.toString(),FB_SECRET.toString());
		facebook.setOAuthPermissions(FB_PERMISSIONS.toString());
		facebook.setOAuthAccessToken(new AccessToken(FB_TOKEN.toString(), null));
		
		return facebook;
	}
	public Twitter getTwWithCredentials(){
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		  .setOAuthConsumerKey(TW_CKEY.toString())
		  .setOAuthConsumerSecret(TW_CSECRET.toString())
		  .setOAuthAccessToken(TW_TOKEN.toString())
		  .setOAuthAccessTokenSecret(TW_TSECRET.toString());
		TwitterFactory tf = new TwitterFactory(cb.build());
		Twitter twitter = tf.getInstance();
		return twitter;
		
	}
}
