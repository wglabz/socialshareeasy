package in.weargenius.gui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JComponent;

public class FomatComponent {

	public FomatComponent(JComponent component,boolean isStatic) {
		// TODO Auto-generated constructor stub
		if(isStatic){
			component.setFont( new Font(Font.SERIF,Font.PLAIN, 25));
			component.setForeground(Color.black);
			component.setOpaque(false);
		}else{
			component.setFont( new Font(Font.SERIF,Font.BOLD, 18));
			component.setForeground(Color.black);
			component.setOpaque(false);
		}

	}

}
