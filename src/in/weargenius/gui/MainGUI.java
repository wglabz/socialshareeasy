package in.weargenius.gui;

import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.PostUpdate;
import in.weargenius.accounts.InitializeAccounts;
import in.weargenius.groups.GetFBGroups;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.DefaultCaret;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

public class MainGUI {

	static JFrame frame;
	
	JCheckBox[] fbGroups=new JCheckBox[20];
	String groupDetails[][]=new GetFBGroups().get();

	JEditorPane contentInput=new JEditorPane();
	JTextField urlInput=new JTextField();
	private static JTextArea console= new JTextArea();
	private JCheckBox tweetIt=new JCheckBox();
	private JCheckBox linkedIn=new JCheckBox();
	private JCheckBox fbShare=new JCheckBox();
	private JCheckBox fbGroupShare=new JCheckBox();
	
	JTextField imageInput=new JTextField();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new MainGUI();
	}

	
	MainGUI(){
		frame= new JFrame();
		frame.setBounds(100,100,870,650);
		//frame.setUndecorated(true);
		frame.getContentPane().setLayout(null);
		//Color backCOlor = Color.BLACK;
		frame.addMouseListener(mouseListener);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.addMouseMotionListener(mouseListener);
		frame.getContentPane().setBackground(Color.white);
	//	frame.setBackground(new Color(backCOlor.getRed(), backCOlor.getGreen(),backCOlor.getBlue(),100));
		//frame.getContentPane().setBackground(new Color(backCOlor.getRed(),backCOlor.getGreen(),backCOlor.getBlue(),100));
	        //frame.setBorder(BorderFactory.createBevelBorder(3));
		//frame.getC;

    	JLabel contentLabel=new JLabel("Content : ");
    	new FomatComponent(contentLabel,false);
    	contentLabel.setOpaque(false);
    	contentLabel.setBounds(20, 0, 100, 50);
    	frame.getContentPane().add(contentLabel);
    	JLabel urlLabel=new JLabel("URL : ");
    	new FomatComponent(urlLabel,false);
    	urlLabel.setOpaque(false);
    	urlLabel.setBounds(20, 255, 100, 50);
    	frame.getContentPane().add(urlLabel);
    	JLabel imageLable=new JLabel("Image : ");
    	new FomatComponent(imageLable,false);
    	imageLable.setOpaque(false);
    	imageLable.setBounds(20, 300, 100, 50);
    	frame.getContentPane().add(imageLable);
    	
    	JLabel facebookLabel=new JLabel("FB Groups: ");
    	new FomatComponent(facebookLabel,false);
    	facebookLabel.setOpaque(false);
    	facebookLabel.setBounds(20, 400, 100, 50);
    	frame.getContentPane().add(facebookLabel);
    	
    	new FomatComponent(contentInput,true);
    	JScrollPane COntenetScrollPane = new JScrollPane();
    	COntenetScrollPane.setOpaque(false);
    	//scrollPane.setForeground(Color.);
    	COntenetScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    	COntenetScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        COntenetScrollPane.getViewport().setOpaque(false);
    	COntenetScrollPane.setBounds(110, 20, 370, 240);
    	DefaultCaret contentCaret = (DefaultCaret) contentInput.getCaret();
    	contentCaret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
       	
    	COntenetScrollPane.setViewportView(contentInput);
    	frame.getContentPane().add(COntenetScrollPane);
    	frame.getContentPane().add(COntenetScrollPane);

    	new FomatComponent(contentInput,true);
    	urlInput.setBounds(110, 270, 370, 30);
    	new FomatComponent(urlInput,true);
    	frame.getContentPane().add(urlInput);
		
    	imageInput.setBounds(110, 310, 370, 30);
    	new FomatComponent(imageInput,true);
    	frame.getContentPane().add(imageInput);
	
    	JButton postButton=new JButton("Share");
    	postButton.setBounds(750, 550, 100, 50);
    	frame.getContentPane().add(postButton);
    	JButton cancelButton=new JButton("Cancel");
    	cancelButton.setOpaque(false);
    	cancelButton.setBounds(640, 550, 100, 50);
    	frame.getContentPane().add(cancelButton);
    	
    	JLabel twitterLabel=new JLabel("Twitter : ");
    	new FomatComponent(twitterLabel,false);
    	twitterLabel.setOpaque(false);
    	twitterLabel.setBounds(20, 350, 100, 20);
    	frame.getContentPane().add(twitterLabel);
       	tweetIt.setBounds(85, 351, 20, 20);
    	new FomatComponent(tweetIt,true);
    	frame.getContentPane().add(tweetIt);
    	
    	JLabel linkedInLabel=new JLabel("LinkedIn : ");
    	new FomatComponent(linkedInLabel,false);
    	linkedInLabel.setBounds(140, 350, 100, 20);
    	frame.getContentPane().add(linkedInLabel);
    	linkedIn.setBounds(220, 351, 20, 20);
    	new FomatComponent(linkedIn,true);
    	frame.getContentPane().add(linkedIn);

    	JLabel facbookLabel=new JLabel("Facebook : ");
    	new FomatComponent(facbookLabel,false);
    	facbookLabel.setBounds(270, 350, 100, 20);
    	frame.getContentPane().add(facbookLabel);
    	fbShare.setBounds(355, 351, 20, 20);
    	new FomatComponent(fbShare,true);
    	frame.getContentPane().add(fbShare);
    	
    	JLabel fbGroupsLable=new JLabel("FB Groups: ");
    	new FomatComponent(fbGroupsLable,false);
    	fbGroupsLable.setBounds(400, 350, 100, 20);
    	frame.getContentPane().add(fbGroupsLable);
    	fbGroupShare.setBounds(490, 351, 20, 20);
    	new FomatComponent(fbGroupShare,true);
    	frame.getContentPane().add(fbGroupShare);

    	//new FomatComponent(console, false);
    	JScrollPane scrollPane = new JScrollPane();
    	scrollPane.setOpaque(false);
    	//scrollPane.setForeground(Color.);
    	console.setOpaque(false);scrollPane.getViewport().setOpaque(false);
    	console.setForeground(Color.black);
    	scrollPane.setBounds(490,20,350,320);
    	scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    	scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
    	DefaultCaret caret = (DefaultCaret) console.getCaret();
    	caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
       	
    	scrollPane.setViewportView(console);
    	frame.getContentPane().add(scrollPane);


    	console.append(">Welcome Social Share V1 #WearGenius.\n");
    	addFBGroups();
		frame.setVisible(true);
    	
		cancelButton.addMouseListener(new MouseAdapter() {
					
					@Override
					public void mouseClicked(MouseEvent arg0) {
						console.append(">Closing in 5 second.\n");

						frame.dispose();
					}
				});
		postButton.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
		    	log("Starting to post.",false);
		    	//console.
		    	postClicked();
				//frame.dispose();
			}
		});
		
	}
	protected void postClicked() {
		console.setForeground(Color.black);
		// TODO Auto-generated method stub
		if(!contentInput.getText().isEmpty()){
			InitializeAccounts fbf= new InitializeAccounts();
			Twitter twitter = fbf.getTwWithCredentials();
			Facebook facebook= fbf.getFBWithCredentials();
			log("Accounts Initialized",false);
			System.out.println(urlInput.getText().isEmpty());
			if(!urlInput.getText().isEmpty()){//Have URL
				if(tweetIt.isSelected()){
					log("Tweeting to twitter with URL",false);
					try {
						Status x=twitter.updateStatus(contentInput.getText()+" "+urlInput.getText());
						log("Tweet ID :"+x.getId(),false);
					} catch (TwitterException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						log("Not abel to tweet currently.",true);
					}
				}
				if(fbShare.isSelected()){
					log("Posting to Facebook Wall with URL",false);
					try {//dhgfjkhgkjhfjkhgjkfgjk
						PostUpdate pu=new PostUpdate(contentInput.getText());
						if(!imageInput.getText().isEmpty())
							pu.setPicture(new URL(imageInput.getText()));
						pu.setLink(new URL(urlInput.getText()));
						//String x = facebook.postLink(new URL(urlInput.getText()), contentInput.getText());
						 String x = facebook.postFeed(pu);
						//facebook.postGroupLink("",new URL(urlInput.getText()),contentInput.getText());
						//facebook.page
						log("Facebook Post ID "+x,false);
					} catch(Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						log("Not able to post to Facebook.",true);
					}
				}
				if(linkedIn.isSelected()){
					log("Posting to LinkedIN with URL.",false);
					try {
						//facebook.postGroupLink("",new URL(urlInput.getText()),contentInput.getText());
					} catch(Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						log("Not able to post to LinkedIN.",true);
					}
				}
				if(fbGroupShare.isSelected()){
					log("Posting to FB Groups with URL.",false);
					for(int i=0;i<groupDetails.length;i++){
						if(fbGroups[i].isSelected()){
							try {
								log("Posting to FB Group "+groupDetails[i][0],false);
								PostUpdate pu=new PostUpdate(contentInput.getText());
								if(!imageInput.getText().isEmpty())
									pu.setPicture(new URL(imageInput.getText()));
								pu.setLink(new URL(urlInput.getText()));
								String x=facebook.postFeed(groupDetails[i][1],pu);
								log("Facebook Post ID "+x,false);
							} catch (MalformedURLException | FacebookException e) {
								// TODO Auto-generated catch block
								log("Not able to post to Facebook GGroup.",true);
								e.printStackTrace();
							}
						}
						}
					}
			}else{//NO URL

				if(tweetIt.isSelected()){
					log("Tweeting to twitter.",false);
					try {
						Status x=twitter.updateStatus(contentInput.getText());
						log("Tweet ID :"+x.getId(),false);
					} catch (TwitterException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						log("Not abel to tweet currently.",true);
					}
				}
				if(fbShare.isSelected()){
					log("Posting to Facebook Wall.",false);
					try {
						String x = facebook.postStatusMessage(contentInput.getText());
						log("Facebook Post ID "+x,false);
					} catch(Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						log("Not able to post to Facebook.",true);
					}
				}
				if(linkedIn.isSelected()){
					log("Posting to LinkedIN.",false);
					try {
						facebook.postGroupLink("",new URL(urlInput.getText()),contentInput.getText());
					} catch(Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						log("Not able to post to LinkedIN.",true);
					}
				}
				if(fbGroupShare.isSelected()){
					log("Posting to FB Groups.",false);
					for(int i=0;i<groupDetails.length;i++){
						if(fbGroups[i].isSelected()){
							log("Posting to FB Group "+groupDetails[i][0],false);
							try {
								String x=facebook.postGroupStatusMessage(groupDetails[i][1],contentInput.getText());
								log("Facebook Post ID "+x,false);
								//facebook.postGroupLink(groupDetails[i][1],new URL(urlInput.getText()),contentInput.getText());
							} catch (Exception e) {
								// TODO Auto-generated catch block
								log("Not able to post to Facebook GGroup.",true);
								e.printStackTrace();
							}
						}
						}
					}	
			}
		}
		else{
			log("Content can't be null. Please ente something.",false);
		}
	}
	public static void log(String message,boolean isError) {
		console.append(">"+message+"\r\n");
		if(isError){
			console.setForeground(Color.red);
		}
	}
	private void addFBGroups() {
		// TODO Auto-generated method stub
		
		JLabel[] fbGroupsLabel=new JLabel[20];
		
		
		for(int fbgCounter = 0;fbgCounter<20;fbgCounter++){

			fbGroupsLabel[fbgCounter]= new JLabel();
			fbGroups[fbgCounter]= new JCheckBox();
			fbGroups[fbgCounter].setSelected(true);
		}	
		

		fbGroups[0].setBounds(110, 400, 20, 20);
		fbGroupsLabel[0].setBounds(130,400, 200, 20);
		fbGroups[1].setBounds(110, 420, 20, 20);
		fbGroupsLabel[1].setBounds(130,420, 200, 20);
		fbGroups[2].setBounds(110, 440, 20, 20);
		fbGroupsLabel[2].setBounds(130,440, 200, 20);
		fbGroups[3].setBounds(110, 460, 20, 20);
		fbGroupsLabel[3].setBounds(130,460, 200, 20);
		fbGroups[4].setBounds(110, 480, 20, 20);
		fbGroupsLabel[4].setBounds(130,480, 200, 20);
		fbGroups[5].setBounds(110, 500, 20, 20);
		fbGroupsLabel[5].setBounds(130,500, 200, 20);
		fbGroups[6].setBounds(110, 520, 20, 20);
		fbGroupsLabel[6].setBounds(130,520, 200, 20);
		fbGroups[7].setBounds(110, 540, 20, 20);
		fbGroupsLabel[7].setBounds(130,540, 200, 20);
		fbGroups[8].setBounds(110, 560, 20, 20);
		fbGroupsLabel[8].setBounds(130,560, 200, 20);
		fbGroups[9].setBounds(110, 580, 20, 20);
		fbGroupsLabel[9].setBounds(130,580, 200, 20);

		fbGroups[10].setBounds(340, 400, 20, 20);
		fbGroupsLabel[10].setBounds(360,400, 200, 20);
		fbGroups[11].setBounds(340, 420, 20, 20);
		fbGroupsLabel[11].setBounds(360,420, 200, 20);
		fbGroups[12].setBounds(340, 440, 20, 20);
		fbGroupsLabel[12].setBounds(360,440, 200, 20);
		fbGroups[13].setBounds(340, 460, 20, 20);
		fbGroupsLabel[13].setBounds(360,460, 200, 20);
		fbGroups[14].setBounds(340, 480, 20, 20);
		fbGroupsLabel[14].setBounds(360,480, 200, 20);
		fbGroups[15].setBounds(340, 500, 20, 20);
		fbGroupsLabel[15].setBounds(360,500, 200, 20);
		fbGroups[16].setBounds(340, 520, 20, 20);
		fbGroupsLabel[16].setBounds(360,520, 200, 20);
		fbGroups[17].setBounds(340, 540, 20, 20);
		fbGroupsLabel[17].setBounds(360,540, 200, 20);
		fbGroups[18].setBounds(340, 560, 20, 20);
		fbGroupsLabel[18].setBounds(360,560, 200, 20);
		fbGroups[19].setBounds(340, 580, 20, 20);
		fbGroupsLabel[19].setBounds(360,580, 200, 20);

		for(int fbgCounter = 0;fbgCounter<groupDetails.length;fbgCounter++){

			fbGroupsLabel[fbgCounter].setText(groupDetails[fbgCounter][0]);
			//fbGroupsLabel[fbgCounter].setText(groupDetails[fbgCounter][0]);
			new FomatComponent(fbGroups[fbgCounter],true);
	    	frame.getContentPane().add(fbGroups[fbgCounter]);
	    	fbGroupsLabel[fbgCounter].setOpaque(false);
	    	fbGroupsLabel[fbgCounter].setForeground(Color.black);
	    	frame.getContentPane().add(fbGroupsLabel[fbgCounter]);
		}
	}
	final static MouseAdapter mouseListener = new MouseAdapter() {
		int x, y;
		@Override
		public void mousePressed(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
		   x = e.getX();
		   y = e.getY();
		}
		}

		@Override
		public void mouseDragged(MouseEvent e) {
		if ((e.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK) != 0) {
			frame. setLocation(e.getXOnScreen() - x, e.getYOnScreen() - y);
		}
		}
		};
		

	    
}
