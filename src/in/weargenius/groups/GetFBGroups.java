package in.weargenius.groups;

import java.io.FileReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import in.weargenius.gui.MainGUI;

public class GetFBGroups {
	public String[][] get(){
				JSONParser parser = new JSONParser();
				 String[][] groupDetails = {{"No Data","No ID"},{"No Data","No ID"},{"No Data","No ID"},{"No Data","No ID"},{"No Data","No ID"}};
				try {

					Object obj = parser.parse(new FileReader("D:\\fbGroups.json"));
					//Format: {"facebook":[{"ID":"4343343"},{"SECRET":"strrt"},{"TOKEN":"dgfgfg"},{"PERMISSIONS":"dhfjdhf"}],"twitter":[{"CKEY":"vdfd"},{"CSECRET":"dfdf"},{"TOKEN":"dfdf"},{"TSECRET":"dfdf"}]}
					JSONArray jsonArray = (JSONArray) obj;
					groupDetails = new String[jsonArray.size()][2];
					for(int counter=0;counter<jsonArray.size();counter++){
						JSONArray elementId=(JSONArray) jsonArray.get(counter);
						groupDetails[counter][0]=((JSONObject)elementId.get(0)).get("NAME").toString();
						groupDetails[counter][1]=((JSONObject)elementId.get(1)).get("ID").toString();
					}

				} catch (Exception e) {
					e.printStackTrace();
					MainGUI.log("Unable to find Facebook group details config file.",true);
					return groupDetails;
				}
			

		return groupDetails;
		}
}
